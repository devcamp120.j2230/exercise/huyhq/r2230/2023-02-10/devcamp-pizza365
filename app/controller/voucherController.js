const { default: mongoose } = require("mongoose");
const voucherModel = require("../models/voucherModel");

const getAllVoucherController = (req, res) => {
    voucherModel.find((error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Co loi xay ra: ${error.message}`
            })
        } else {
            return res.status(200).json({
                message: `Lay du lieu thanh cong!`,
                voucher: data
            })
        }
    })
};

const getAVoucherByMa = (req, res) => {
    var maVoucher = req.params.maVoucher;
    voucherModel.findOne({maVoucher}, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Co loi xay ra: ${error.message}`
            })
        } else {
            if(data){
                return res.status(200).json({
                    message: `Lay du lieu thanh cong!`,
                    data
                })
            }else{
                return res.status(500).json({
                    message: `Khong ton tai voucher!`,
                    data: null
                })
            }
            
        }
    })
};

const postAVoucherController = (req, res) => {
    var body = req.body;
    if (!body.maVoucher) {
        return res.status(400).json({
            message: "Ma voucher phai bat buoc!"
        })
    }

    if (!body.phanTramGiamGia) {
        return res.status(400).json({
            message: "Phan tram giam gia phai bat buoc!"
        })
    }

    if (!Number.isInteger(body.phanTramGiamGia) || body.phanTramGiamGia < 0 || body.phanTramGiamGia > 100) {
        return res.status(400).json({
            message: "Phan tram giam gia khong dung dinh dang!"
        })
    }

    const newVoucher = new voucherModel({
        _id: mongoose.Types.ObjectId(),
        maVoucher: body.maVoucher,
        phanTramGiamGia: body.phanTramGiamGia,
        ghiChu: body.ghiChu
    })

    voucherModel.create(newVoucher, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Co loi xay ra: ${error.message}`
            })
        } else {
            return res.status(201).json({
                message: `Tao moi voucher thanh cong!`,
                voucher: data
            })
        }
    })
};

const putAVoucherController = (req, res) => {
    var id = req.params.id;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: 'Id khong dung dinh dang!',
        })
    }

    var body = req.body;
    if (!body.maVoucher) {
        return res.status(400).json({
            message: "Ma voucher phai bat buoc!"
        })
    }

    if (!body.phanTramGiamGia) {
        return res.status(400).json({
            message: "Phan tram giam gia phai bat buoc!"
        })
    }

    if (!Number.isInteger(body.phanTramGiamGia) || body.phanTramGiamGia < 0 || body.phanTramGiamGia > 100) {
        return res.status(400).json({
            message: "Phan tram giam gia khong dung dinh dang!"
        })
    }
    
    const voucher = new voucherModel({
        maVoucher: body.maVoucher,
        phanTramGiamGia: body.phanTramGiamGia,
        ghiChu: body.ghiChu
    })

    voucherModel.findByIdAndUpdate(id, voucher, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Co loi xay ra: ${error.message}`
            })
        } else {
            return res.status(201).json({
                message: `Sua thanh cong voucher id: ${id}`,
                voucher: data
            })
        }
    })
};

const deleteAVoucherController = (req, res) => {
    var id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: 'Id khong dung dinh dang!',
        })
    }

    voucherModel.findByIdAndDelete(id, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Co loi xay ra: ${error.message}`
            })
        } else {
            return res.status(201).json({
                message: `Xoa thanh cong voucher id: ${id}`,
                voucher: data
            })
        }
    })
};

module.exports = {
    getAllVoucherController,
    getAVoucherByMa,
    postAVoucherController,
    putAVoucherController,
    deleteAVoucherController
}