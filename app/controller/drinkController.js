const {default: mongoose} = require("mongoose");
const drinkModel = require("../models/drinkModel");

const getAllDrinkController = (req,res)=>{
    drinkModel.find((error, data) => {
        if(error){
            return res.status(500).json({
                message : `Co loi xay ra: ${error.message}`
            })
        }else{
            return res.status(200).json({
                message : `Lay thong tin thanh cong!`,
                data
            })
        }
    })
};

const getADrinkController = (req,res)=>{
    var id = req.params.id;
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message : "Id do uong khong dung dinh dang!"
        })
    }

    drinkModel.findById(id, (error, data) => {
        if(error){
            return res.status(500).json({
                message : `Co loi xay ra: ${error.message}`
            })
        }else{
            return res.status(200).json({
                message : `Lay thanh cong thong tin id: ${id}`,
                drink: data
            })
        }
    })
};

const postADrinkController = (req,res)=>{
    var body = req.body;
    if(!body.maNuocUong){
        return res.status(400).json({
            message : "Ma do uong phai bat buoc!"
        })
    }
    if(!body.tenNuocUong){
        return res.status(400).json({
            message : "Ten do uong phai bat buoc!"
        })
    }

    if(!body.donGia){
        return res.status(400).json({
            message : "Don gian do uong phai bat buoc!"
        })
    }

    if(!Number.isInteger(body.donGia) || body.donGia < 0){
        return res.status(400).json({
            message : "Don gian do uong khong dung dinh dang!"
        })
    }
    // res.status(200).json({
    //     body
    // })
    var newDrink = new drinkModel({
        _id : mongoose.Types.ObjectId(),
        maNuocUong: body.maNuocUong,
        tenNuocUong: body.tenNuocUong,
        donGia: body.donGia,
    });

    drinkModel.create(newDrink,(error, data) => {
        if(error){
            return res.status(500).json({
                message : `Co loi xay ra: ${error.message}`
            })
        }else{
            return res.status(201).json({
                message : `Tao moi do uong thanh cong!`,
                drink: data
            })
        }
    })
};

const putADrinkController = (req,res)=>{
    var id = req.params.id;
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message : "Id do uong khong dung dinh dang!"
        })
    }
    var body = req.body;
    if(!body.maNuocUong){
        return res.status(400).json({
            message : "Ma do uong phai bat buoc!"
        })
    }
    if(!body.tenNuocUong){
        return res.status(400).json({
            message : "Ten do uong phai bat buoc!"
        })
    }

    if(!body.donGia){
        return res.status(400).json({
            message : "Don gian do uong phai bat buoc!"
        })
    }

    var drink = new drinkModel({
        maNuocUong: body.maNuocUong,
        tenNuocUong: body.tenNuocUong,
        donGia: body.donGia,
    });

    drinkModel.findByIdAndUpdate(id, drink, (error, data) => {
        if(error){
            return res.status(500).json({
                message : `Co loi xay ra: ${error.message}`
            })
        }else{
            return res.status(201).json({
                message : `Sua do uong thanh cong!`,
                drink: data
            })
        }
    })
};

const deleteADrinkController = (req,res)=>{
    var id = req.params.id;
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message : "Id do uong khong dung dinh dang!"
        })
    }
    drinkModel.findByIdAndDelete(id, (error, data) => {
        if(error){
            return res.status(500).json({
                message : `Co loi xay ra: ${error.message}`
            })
        }else{
            return res.status(204).json({
                message : `Xoa thanh cong do uong id: ${id}`,
                drink: data
            })
        }
    })
};

module.exports = {
    getAllDrinkController,
    getADrinkController,
    postADrinkController,
    putADrinkController,
    deleteADrinkController
};