const mongoose = require("mongoose");
const randtoken = require("rand-token");

const Schema = mongoose.Schema;

const orderSchema = new Schema({
    _id: mongoose.Types.ObjectId,

    orderCode: {
        type: String,
        unique: true,
        default: function () {
            // return Math.floor(Math.random() * 1000000) + 1000000;
            return randtoken.generate(10);                              //tạo một token ngẫu nhiên 10 ký tự
        }
    },

    pizzaSize: {
        type: String,
        required: true,
    },

    pizzaType: {
        type: String,
        required: true,
    },

    voucher: {
        type: mongoose.Types.ObjectId,
        ref: "voucher"
    },

    drink: {
        type: mongoose.Types.ObjectId,
        ref: "drink"
    },

    status: {
        type: String,
        required: true,
    },
});

module.exports = mongoose.model("order", orderSchema)