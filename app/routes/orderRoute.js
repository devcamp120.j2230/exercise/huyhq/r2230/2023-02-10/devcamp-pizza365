const express = require('express');

const {
    createOrderByRequest,
    createOrder,
    getAllOrder,
    getOrderById,
    updateOrderById,
    deleteOrderById
} = require('../controller/orderController');

const orderRouter = express.Router();

orderRouter.post('/orders', createOrderByRequest);
orderRouter.post('/user/:userId/order', createOrder);
orderRouter.get('/user/:userId/order', getAllOrder);
orderRouter.get('/order/:orderId', getOrderById);
orderRouter.put('/order/:orderId', updateOrderById);
orderRouter.delete('/user/:userId/order/:orderId', deleteOrderById);

module.exports = {orderRouter};