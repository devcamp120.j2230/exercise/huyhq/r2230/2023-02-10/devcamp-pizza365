const express = require("express");

//get middleware
const {
    getAllDrink,
    getADrink,
    postADrink,
    putADrink,
    deleteADrink
} = require("../middleware/drinkMiddleware");


//get controller
const {
    getAllDrinkController,
    getADrinkController,
    postADrinkController,
    putADrinkController,
    deleteADrinkController
} = require("../controller/drinkController");

//khai bao route
const drinkRoute = express.Router();

drinkRoute.get("/drinks",getAllDrink,getAllDrinkController)

drinkRoute.post("/drink",postADrink,postADrinkController)

drinkRoute.get("/drink/:id",getADrink,getADrinkController)

drinkRoute.put("/drink/:id",putADrink,putADrinkController)

drinkRoute.delete("/drink/:id",deleteADrink,deleteADrinkController)

module.exports = {drinkRoute};