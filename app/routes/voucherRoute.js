const express = require("express");

const {
    getAllVoucher,
    getAVoucher,
    postAVoucher,
    putAVoucher,
    deleteAVoucher
} = require("../middleware/voucherMiddleware");

const {
    getAllVoucherController,
    getAVoucherByMa,
    postAVoucherController,
    putAVoucherController,
    deleteAVoucherController
} = require("../controller/voucherController");
const voucherRoute = express.Router();


voucherRoute.get("/voucher", getAllVoucher, getAllVoucherController)

voucherRoute.post("/voucher", postAVoucher, postAVoucherController)

voucherRoute.get("/voucher/:maVoucher", getAVoucher, getAVoucherByMa)

voucherRoute.put("/voucher/:id", putAVoucher, putAVoucherController)

voucherRoute.delete("/voucher/:id", deleteAVoucher, deleteAVoucherController)

module.exports = { voucherRoute };